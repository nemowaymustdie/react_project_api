package kg.oblaka.oblaka.repository;

import kg.oblaka.oblaka.domain.DoctorImage;

public interface DoctorImageRepository extends BaseRepository<DoctorImage>{
}
