package kg.oblaka.oblaka.repository;

import kg.oblaka.oblaka.domain.Clinic;
import org.springframework.stereotype.Repository;

@Repository
public interface ClinicRepository extends BaseRepository<Clinic> {
}
