package kg.oblaka.oblaka.repository;

import kg.oblaka.oblaka.domain.Accommodation;
import org.springframework.stereotype.Repository;

@Repository
public interface AccommodationRepository extends BaseRepository<Accommodation> {
}
