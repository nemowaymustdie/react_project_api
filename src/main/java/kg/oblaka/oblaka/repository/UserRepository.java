package kg.oblaka.oblaka.repository;

import kg.oblaka.oblaka.domain.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User> {

    @Query("{'name': ?0}")
    Optional<User> findByName(String name);
}
