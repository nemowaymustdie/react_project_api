package kg.oblaka.oblaka.repository;

import kg.oblaka.oblaka.domain.Doctor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DoctorRepository extends BaseRepository<Doctor> {



    Iterable<Doctor> findAllOrderByPosition();
}
