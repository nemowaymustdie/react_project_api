package kg.oblaka.oblaka.repository;

import kg.oblaka.oblaka.domain.Branch;

public interface BranchRepository extends BaseRepository<Branch> {
}
