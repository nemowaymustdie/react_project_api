package kg.oblaka.oblaka.repository;

import kg.oblaka.oblaka.domain.Share;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRepository extends BaseRepository<Share> {
}
