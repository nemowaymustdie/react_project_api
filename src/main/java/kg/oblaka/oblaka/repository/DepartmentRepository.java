package kg.oblaka.oblaka.repository;

import kg.oblaka.oblaka.domain.Department;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends BaseRepository<Department> {
}

