package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.Doctor;
import kg.oblaka.oblaka.domain.DoctorImage;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;


@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class DoctorImageDTO {

    public static List<DoctorImageDTO> toListDTOS(List<DoctorImage> doctorImages) {
        List<DoctorImageDTO> doctorDTOS = new ArrayList<>();
        for(DoctorImage doctorImage : doctorImages){
            doctorDTOS.add(DoctorImageDTO.toDTO(doctorImage));
        }
        return doctorDTOS;
    }

    public static DoctorImageDTO toDTO(DoctorImage doctorImage){
        return builder()
                .id(doctorImage.getId())

                .imageData(doctorImage.getImageData())
                .doctor(doctorImage.getDoctor())
                .build();
    }



    private String id;
    private byte[] imageData;
    private Doctor doctor;
}
