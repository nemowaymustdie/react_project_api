package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.*;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class DepartmentDTO {


    public static DepartmentDTO toDTO(Department department){
        return builder().id(department.getId()).title(department.getTitle()).branch(department.getBranch()).build();
    }


    public static List<DepartmentDTO> toListDTOS(List<Department> departments){
        List<DepartmentDTO> departmentDTOS = new ArrayList<>();
        for(Department department: departments){
            departmentDTOS.add(DepartmentDTO.toDTO(department));
        }
        return departmentDTOS;
    }

    private String id;
    private String title;
    private List<Branch> branch;
}
