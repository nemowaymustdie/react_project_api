package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.Accommodation;
import kg.oblaka.oblaka.domain.Clinic;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class AccommodationDTO {

    public static List<AccommodationDTO> toListDTOS(List<Accommodation> accommodations) {
        List<AccommodationDTO> accommodationDTOS = new ArrayList<>();
        for(Accommodation accommodation : accommodations){
            accommodationDTOS.add(AccommodationDTO.toDTO(accommodation));
        }
        return accommodationDTOS;
    }

    public static AccommodationDTO toDTO(Accommodation accommodation){
        return builder()
                .id(accommodation.getId())
                .text(accommodation.getText())
                .minPrice(accommodation.getMinPrice())
                .maxPrice(accommodation.getMaxPrice())
                .build();
    }


    private String id;

    private String text;
    private Integer minPrice;
    private Integer maxPrice;

    public static Accommodation toModel(AccommodationDTO accommodationDTO) {
        return Accommodation.builder().text(accommodationDTO.getText()).id(accommodationDTO.getId()).minPrice(accommodationDTO.getMinPrice()).maxPrice(accommodationDTO.getMaxPrice()).build();
    }
}
