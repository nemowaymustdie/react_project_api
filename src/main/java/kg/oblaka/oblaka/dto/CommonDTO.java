package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.Clinic;
import kg.oblaka.oblaka.domain.Comment;
import kg.oblaka.oblaka.domain.Doctor;
import kg.oblaka.oblaka.domain.Share;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CommonDTO {

    private List<ClinicDTO> clinic;
    private List<DoctorDTO> doctor;
    private List<ShareDTO> share;
    private List<CommentDTO> comment;
    private List<DepartmentDTO> department;


    public static CommonDTO toDTO(List<ClinicDTO> clinicDTOS, List<DoctorDTO> doctorDTOS, List<DepartmentDTO> departmentDTOS, List<ShareDTO> shareDTOS, List<CommentDTO> commentDTOS) {
        return builder()
                .clinic(clinicDTOS)
                .doctor(doctorDTOS)
                .department(departmentDTOS)
                .share(shareDTOS)
                .comment(commentDTOS)
                .build();
    }
}
