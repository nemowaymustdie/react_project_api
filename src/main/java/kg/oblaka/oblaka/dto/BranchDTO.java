package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.Accommodation;
import kg.oblaka.oblaka.domain.Branch;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class BranchDTO {


    public static List<BranchDTO> toListDTOS(List<Branch> branches) {
        List<BranchDTO> branchDTOS = new ArrayList<>();
        for(Branch branch : branches){
            branchDTOS.add(BranchDTO.toDTO(branch));
        }
        return branchDTOS;
    }

    public static BranchDTO toDTO(Branch branch){
        return builder()
                .id(branch.getId())
                .title(branch.getTitle())
                .accommodations(branch.getAccommodations())
                .build();
    }

    private String id;
    private String title;
    private List<Accommodation> accommodations;
}
