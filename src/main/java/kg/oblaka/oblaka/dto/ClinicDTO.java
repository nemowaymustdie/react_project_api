package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.Clinic;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class ClinicDTO {
    public static List<ClinicDTO> toListDTOS(List<Clinic> clinics) {
        List<ClinicDTO> clinicDTOS = new ArrayList<>();
        for(Clinic clinic : clinics){
            clinicDTOS.add(ClinicDTO.toDTO(clinic));
        }
        return clinicDTOS;
    }

    public static ClinicDTO toDTO(Clinic clinic){
        return builder()
                .id(clinic.getId())
                .title(clinic.getTitle())
                .address(clinic.getAddress())
                .telForCall(clinic.getTelForCall())
                .image(clinic.getImage())
                .description(clinic.getDescription())
                .number(clinic.getNumber())
                .video(clinic.getVideo())
                .email(clinic.getEmail())
                .build();
    }

    private String id;
    private String title;
    private String telForCall;
    private String image;
    private String video;
    private String email;

    private String address;
    private String number;
    private String description;
}
