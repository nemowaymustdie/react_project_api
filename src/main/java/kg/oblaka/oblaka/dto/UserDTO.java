package kg.oblaka.oblaka.dto;


import kg.oblaka.oblaka.domain.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDTO {
    public static UserDTO from(User user) {

        return builder()
                .id(user.getId())
                .name(user.getName())
                .build();
    }

    private String id;
    private String name;
    private String password;
    private String token;



}
