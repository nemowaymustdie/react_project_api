package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.Comment;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CommentDTO {

    public static CommentDTO toDTO(Comment comment){
        return builder().id(comment.getId()).name(comment.getName()).text(comment.getText()).build();
    }

    public static Comment toModel(CommentDTO commentDTO){
        return Comment.builder().id(commentDTO.getId()).name(commentDTO.getName()).text(commentDTO.getText()).build();
    }

    public static List<CommentDTO> toListDTO(List<Comment> comments){
        List<CommentDTO> commentDTOS = new ArrayList<>();
        for(Comment comment: comments){
            commentDTOS.add(CommentDTO.toDTO(comment));
        }
        return commentDTOS;
    }

    private String id;
    private String name;
    private String text;
}
