package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.Clinic;
import kg.oblaka.oblaka.domain.Comment;
import kg.oblaka.oblaka.domain.Doctor;
import kg.oblaka.oblaka.repository.DoctorImageRepository;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class DoctorDTO {

    public static List<DoctorDTO> toListDTOS(List<Doctor> doctors) {
        List<DoctorDTO> doctorDTOS = new ArrayList<>();
        for(Doctor doctor : doctors){
            doctorDTOS.add(DoctorDTO.toDTO(doctor));
        }
        return doctorDTOS;
    }

    public static DoctorDTO toDTO(Doctor doctor){
        return builder()
                .id(doctor.getId())
                .position(doctor.getPosition())
                .name(doctor.getName())
                .description(doctor.getDescription())
                .photoId(doctor.getDoctorImage().getId())
                .build();
    }


    private String id;
    private Integer position;

    private String name;
    private String photoId;
    private String description;

    public static Doctor toModel(DoctorDTO doctorDTO) {
        return Doctor.builder().id(doctorDTO.getId()).position(doctorDTO.getPosition()).name(doctorDTO.getName()).description(doctorDTO.getDescription()).build();
    }
}
