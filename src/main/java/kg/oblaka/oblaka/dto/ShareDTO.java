package kg.oblaka.oblaka.dto;

import kg.oblaka.oblaka.domain.Comment;
import kg.oblaka.oblaka.domain.Share;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class ShareDTO {

    public static ShareDTO toDTO(Share share){
        return builder().id(share.getId()).price(share.getPrice()).title(share.getTitle()).descriptions(share.getDescriptions()).image(share.getImage()).build();
    }

    public static Share toModel(ShareDTO shareDTO){
        return Share.builder()
                .id(shareDTO.getId())
                .title(shareDTO.getTitle())
                .descriptions(shareDTO.getDescriptions())
                .image(shareDTO.getImage())
                .price(shareDTO.getPrice()).build();
    }


    public static List<ShareDTO> toListDTO(List<Share> shares){
        List<ShareDTO> ShareDTOS = new ArrayList<>();
        for(Share share: shares){
            ShareDTOS.add(ShareDTO.toDTO(share));
        }
        return ShareDTOS;
    }

    private String id;
    private Integer price;

    private String image;
    private String title;
    private List<String> descriptions;
}
