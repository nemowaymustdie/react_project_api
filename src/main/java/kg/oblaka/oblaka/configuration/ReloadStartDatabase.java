package kg.oblaka.oblaka.configuration;

import kg.oblaka.oblaka.domain.*;
import kg.oblaka.oblaka.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@RequiredArgsConstructor
@Configuration
public class ReloadStartDatabase {

    private final AccommodationRepository accommodationRepository;
    private final DepartmentRepository departmentRepository;
    private final CommentRepository commentRepository;
    private final ClinicRepository clinicRepository;
    private final DoctorRepository doctorRepository;
    private final BranchRepository branchRepository;
    private final ShareRepository shareRepository;


    @Bean
    CommandLineRunner initDatabase(){
        return (args) -> {
            comments().forEach(commentRepository::save);
            clinics().forEach(clinicRepository::save);
            doctors().forEach(doctorRepository::save);
            shares().forEach(shareRepository::save);
            accommodations().forEach(accommodationRepository::save);
            branches().forEach(branchRepository::save);
            departments().forEach(departmentRepository::save);
        };
    }

    private List<Comment> comments() {
        commentRepository.deleteAll();
        return List.of(
                Comment.builder().name("Олег").text("Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolorem illum, numquam consectetur placeat facere quis ipsa molestias. Reiciendis obcaecati laboriosam iusto atque culpa iure laLorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolorem illum, numquam consectetur placeat facere quis ipsa molestias. Reiciendis obcaecati laboriosam iusto atque culpa iure laudantiumest sequi iste excepturi.udantiumest sequi iste excepturi.").build(),
                Comment.builder().name("Олег").text("lorem").build(),
                Comment.builder().name("Олег").text("lorem").build()
        );
    }

    private List<Clinic> clinics(){
        clinicRepository.deleteAll();
        return List.of(
                Clinic.builder().title("Облака").telForCall("996999961313").address("Грибоедова 16, 1 этаж").email("oblaka.clinic@gmail.com").number("+996 (999) 96 13 13").video("https://www.youtube-nocookie.com/embed/7Qg6NrniXoI?controls=0").build()
        );
    }

    private List<Doctor> doctors(){
        doctorRepository.deleteAll();
        return List.of(
                Doctor.builder().position(3).name("Антонова Анастасия").description("Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolorem illum, numquam consectetur placeat facere quis ipsa molestias. Reiciendis obcaecati laboriosam iusto atque culpa iure laudantiumest sequi iste excepturi.").build(),
                Doctor.builder().position(1).name("Антонова Анастасия").description("Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolorem illum, numquam consectetur placeat facere quis ipsa molestias. Reiciendis obcaecati laboriosam iusto atque culpa iure laudantiumest sequi iste excepturi.").build(),
                Doctor.builder().position(2).name("Антонова Анастасия").description("Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolorem illum, numquam consectetur placeat facere quis ipsa molestias. Reiciendis obcaecati laboriosam iusto atque culpa iure laudantiumest sequi iste excepturi.").build()
        );
    }


    private List<Share> shares(){
        shareRepository.deleteAll();
        return List.of(
                Share.builder().title("Профессиональная гигиена полости рта").descriptions(List.of("проводиться врачами-терапевтами", "на оборудовании европейских стандартов(KaVa, Германия)", "строго калиброванным порошком с мягким гранулами", "они бережно очищают налет, не царапая эмаль")).price(2000).build(),
                Share.builder().title("Профессиональная гигиена полости рта").descriptions(List.of("проводиться врачами-терапевтами", "на оборудовании европейских стандартов(KaVa, Германия)", "строго калиброванным порошком с мягким гранулами", "они бережно очищают налет, не царапая эмаль")).price(2000).build(),
                Share.builder().title("Профессиональная гигиена полости рта").descriptions(List.of("проводиться врачами-терапевтами", "на оборудовании европейских стандартов(KaVa, Германия)", "строго калиброванным порошком с мягким гранулами", "они бережно очищают налет, не царапая эмаль")).price(2000).build()
        );
    }


    private List<Accommodation> accommodations(){
        accommodationRepository.deleteAll();
        return List.of(
                Accommodation.builder().text("Изготовление и анализ гибсовых моделей фотопротокол").minPrice(1500).build()
        );
    }

    private List<Branch> branches(){
        branchRepository.deleteAll();
        return List.of(
                Branch.builder().title("Ортодонтическая диагностика").accommodations((List<Accommodation>) accommodationRepository.findAll()).build()
        );
    }

    private List<Department> departments(){
        departmentRepository.deleteAll();
        return List.of(
                Department.builder().title("Ортодонтия").branch((List<Branch>) branchRepository.findAll()).build(),
                Department.builder().title("Ортодонтия").branch((List<Branch>) branchRepository.findAll()).build(),
                Department.builder().title("Ортодонтия").branch((List<Branch>) branchRepository.findAll()).build()
        );
    }




}
