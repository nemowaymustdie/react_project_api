package kg.oblaka.oblaka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OblakaApplication {

    public static void main(String[] args) {
        SpringApplication.run(OblakaApplication.class, args);
    }

}
