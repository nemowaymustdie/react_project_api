package kg.oblaka.oblaka.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Document("comments")
public class Comment {
    @Id
    private String id;
    private String name;
    private String text;
}
