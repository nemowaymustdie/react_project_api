package kg.oblaka.oblaka.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Document("accommodations")
public class Accommodation {

    @Id
    private String id;

    private String text;
    private Integer minPrice;
    private Integer maxPrice;
}
