package kg.oblaka.oblaka.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Document("shares")
public class Share {

    @Id
    private String id;
    private Integer price;

    private String image;
    private String title;
    private List<String> descriptions;
}
