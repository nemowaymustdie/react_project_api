package kg.oblaka.oblaka.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Document("clinics")
public class Clinic {

    @Id
    private String id;

    private String title;

    private String image;
    private String video;


    private String email;
    private String address;
    private String number;
    private String telForCall;
    private String description;
}
