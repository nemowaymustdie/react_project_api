package kg.oblaka.oblaka.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Document("departments")
public class Department {

    @Id
    private String id;

    private String title;

    @DBRef
    private List<Branch> branch;
}
