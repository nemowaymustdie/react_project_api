package kg.oblaka.oblaka.domain;

import kg.oblaka.oblaka.dto.UserDTO;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Data
@Builder
public class AuthenticationResponse {
    public static AuthenticationResponse from (UserDTO dto, String token) {
        return builder()
                .id(dto.getId())
                .username(dto.getName())
                .token(token)
                .build();
    }

    private String id;
    private String username;
    private String token;

}