package kg.oblaka.oblaka.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Document("doctors")
public class Doctor {

    @Id
    private String id;
    private Integer position;

    private String name;
    @DBRef
    private DoctorImage doctorImage = DoctorImage.EMPTY;
    private String description;
}
