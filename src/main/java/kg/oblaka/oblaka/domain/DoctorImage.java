package kg.oblaka.oblaka.domain;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Data
@Builder
@Document("doctorImages")
public class DoctorImage {
    public static final DoctorImage EMPTY = builder().build();

    @Id
    private String id;

    private byte[] imageData = new byte[0];

    @DBRef
    private Doctor doctor;
}
