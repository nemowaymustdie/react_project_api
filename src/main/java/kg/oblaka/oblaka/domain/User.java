package kg.oblaka.oblaka.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Document("users")
public class User {
    public static final User EMPTY = builder().name("test").password("test").build();
    @Id
    private String id;

    @Indexed
    private String name;
    private String password;

}
