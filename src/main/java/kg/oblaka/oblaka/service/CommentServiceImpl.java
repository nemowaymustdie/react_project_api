package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.Comment;
import kg.oblaka.oblaka.dto.CommentDTO;
import kg.oblaka.oblaka.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;

    @Override
    public List<CommentDTO> show() {
        return CommentDTO.toListDTO((List<Comment>) commentRepository.findAll());
    }

    @Override
    public boolean addComment(CommentDTO commentDTO) {
        Comment comment = CommentDTO.toModel(commentDTO);
        if(comment.getId() == null && comment.getName() != null && comment.getText() != null){
            commentRepository.save(comment);
            return true;
        }else{
            return false;
        }

    }


    @Override
    public ResponseEntity<Void> delete(String id) {
        Optional<Comment> comment = commentRepository.findById(id);
        try{
            if (!comment.isEmpty()) {
                commentRepository.delete(comment.get());
                return ResponseEntity.noContent().build();
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }

    }

    @Override
    public ResponseEntity<Void> update(String id, CommentDTO commentDTO) {
        Optional<Comment> comment = commentRepository.findById(id);
        try{
            if (!comment.isEmpty()) {
                Comment changedComment = comment.get();
                changedComment.setName(commentDTO.getName());
                changedComment.setText(commentDTO.getText());
                commentRepository.save(changedComment);
                return ResponseEntity.noContent().build();
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public CommentDTO showById(String id) {
        return CommentDTO.toDTO(commentRepository.findById(id).get());
    }
}
