package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.Doctor;
import kg.oblaka.oblaka.domain.DoctorImage;
import kg.oblaka.oblaka.domain.Share;
import kg.oblaka.oblaka.dto.ClinicDTO;
import kg.oblaka.oblaka.dto.DoctorDTO;
import kg.oblaka.oblaka.dto.ShareDTO;
import kg.oblaka.oblaka.repository.DoctorImageRepository;
import kg.oblaka.oblaka.repository.DoctorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;
    private final DoctorImageRepository doctorImageRepository;

    @Override
    public List<DoctorDTO> show() {
        List<Doctor> doctors = (List<Doctor>) doctorRepository.findAll();
        doctors.sort(Comparator.comparing(Doctor::getPosition));
        return DoctorDTO.toListDTOS(doctors);
    }

    @Override
    public boolean add(DoctorDTO doctorDTO) {
        Doctor doctor = DoctorDTO.toModel(doctorDTO);
        if(doctor.getId() == null && doctor.getDescription() != null && doctor.getName() != null && doctor.getDoctorImage() != null){
            doctorRepository.save(doctor);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public ResponseEntity<Void> update(String id, DoctorDTO doctorDTO) {
        Optional<Doctor> doctor = doctorRepository.findById(id);
        Optional<DoctorImage> doctorImage = doctorImageRepository.findById(doctorDTO.getPhotoId());

        try{

            if (!doctor.isEmpty()) {
                Doctor changedDoctor = doctor.get();
                changedDoctor.setPosition(doctorDTO.getPosition());
                changedDoctor.setDescription(doctorDTO.getDescription());
                changedDoctor.setName(doctorDTO.getName());
                changedDoctor.setDoctorImage(doctorImage.get());
                doctorRepository.save(changedDoctor);
                return ResponseEntity.noContent().build();
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<Void> delete(String id) {
        Optional<Doctor> doctor = doctorRepository.findById(id);
        try{
            if (!doctor.isEmpty()) {
                doctorRepository.delete(doctor.get());
                return ResponseEntity.noContent().build();
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public DoctorDTO showById(String id) {
        return DoctorDTO.toDTO(doctorRepository.findById(id).get());
    }
}
