package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.Comment;
import kg.oblaka.oblaka.domain.Share;
import kg.oblaka.oblaka.dto.CommentDTO;
import kg.oblaka.oblaka.dto.ShareDTO;
import kg.oblaka.oblaka.repository.ShareRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ShareServiceImpl implements ShareService {
    private final ShareRepository shareRepository;


    @Override
    public List<ShareDTO> show() {
        return ShareDTO.toListDTO((List<Share>) shareRepository.findAll());
    }

    @Override
    public boolean addShare(ShareDTO shareDTO) {
        Share share = ShareDTO.toModel(shareDTO);
        if(share.getId() == null && share.getDescriptions() != null && share.getTitle() != null && share.getPrice() != null){
            shareRepository.save(share);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public ResponseEntity<Void> update(String id, ShareDTO shareDTO) {
        Optional<Share> share = shareRepository.findById(id);
        try{
            if (!share.isEmpty()) {
                Share changedShare = share.get();
                changedShare.setTitle(shareDTO.getTitle());
                changedShare.setDescriptions(shareDTO.getDescriptions());
                changedShare.setPrice(shareDTO.getPrice());
                changedShare.setImage(shareDTO.getImage());
                shareRepository.save(changedShare);
                return ResponseEntity.noContent().build();
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<Void> delete(String id) {
        Optional<Share> share = shareRepository.findById(id);
        try{
            if (!share.isEmpty()) {
                shareRepository.delete(share.get());
                return ResponseEntity.noContent().build();
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }
}
