package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.CommentDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CommentService {
    List<CommentDTO> show();

    boolean addComment(CommentDTO commentDTO);

    ResponseEntity<Void> delete(String id);

    ResponseEntity<Void> update(String id, CommentDTO commentDTO);

    CommentDTO showById(String id);
}
