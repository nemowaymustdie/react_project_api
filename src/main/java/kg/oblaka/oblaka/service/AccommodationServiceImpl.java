package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.Accommodation;
import kg.oblaka.oblaka.domain.Doctor;
import kg.oblaka.oblaka.dto.AccommodationDTO;
import kg.oblaka.oblaka.dto.DoctorDTO;
import kg.oblaka.oblaka.repository.AccommodationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AccommodationServiceImpl implements AccommodationService {
    private final AccommodationRepository accommodationRepository;

    @Override
    public List<AccommodationDTO> show() {
        return AccommodationDTO.toListDTOS((List<Accommodation>) accommodationRepository.findAll());
    }

    @Override
    public boolean add(AccommodationDTO accommodationDTO) {
        Accommodation accommodation = AccommodationDTO.toModel(accommodationDTO);
        if(accommodation.getId() == null && accommodation.getText() != null && accommodation.getMinPrice() != null){
            accommodationRepository.save(accommodation);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public ResponseEntity<Void> update(String id, AccommodationDTO accommodationDTO) {
        Optional<Accommodation> accommodation = accommodationRepository.findById(id);
        try{
            if (!accommodation.isEmpty()) {
                Accommodation changedAccommodation = accommodation.get();
                changedAccommodation.setText(accommodationDTO.getText());
                changedAccommodation.setMinPrice(accommodationDTO.getMinPrice());
                changedAccommodation.setMaxPrice(accommodationDTO.getMaxPrice());
                accommodationRepository.save(changedAccommodation);
                return ResponseEntity.noContent().build();
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<Void> delete(String id) {
        Optional<Accommodation> accommodation = accommodationRepository.findById(id);
        try{
            if (!accommodation.isEmpty()) {
                accommodationRepository.delete(accommodation.get());
                return ResponseEntity.noContent().build();
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public AccommodationDTO findById(String id) {
        return AccommodationDTO.toDTO(accommodationRepository.findById(id).get());
    }
}
