package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.ClinicDTO;

import java.util.List;

public interface ClinicService {
    List<ClinicDTO> show();
}
