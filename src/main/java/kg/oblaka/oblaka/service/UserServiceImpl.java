package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.AuthenticationRequest;
import kg.oblaka.oblaka.domain.AuthenticationResponse;
import kg.oblaka.oblaka.domain.User;
import kg.oblaka.oblaka.dto.UserDTO;
import kg.oblaka.oblaka.repository.UserRepository;
import kg.oblaka.oblaka.utils.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService  {

    private final PasswordEncoder passwordEncoder;


    private final AuthenticationManager authenticationManager;

    private final JwtUtil jwtTokenUtil;

    private final UserRepository userRepository;

    public User findById (String userId) {
        return userRepository.findById(userId).orElse(User.EMPTY);
    }

    public UserDTO findByUsername (String username) {
        User user = userRepository.findByName(username).get();

        return UserDTO.from(user);
    }

    public ResponseEntity<?> authenticate (AuthenticationRequest request) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getName(), request.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

//        final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());
//        final String jwt = jwtTokenUtil.generateToken(userDetails);
//        return ResponseEntity.ok(new AuthenticationResponse(jwt));

        final UserDTO dto = findByUsername(request.getName());
        final String token = jwtTokenUtil.generateToken(dto);

        return ResponseEntity.ok(AuthenticationResponse.from(dto, token));
    }

    public UserDTO save (UserDTO userData) {
        User user = User
                .builder()
                .name(userData.getName())
                .password(passwordEncoder.encode(userData.getPassword()))
                .build();

        userRepository.save(user);

        return UserDTO.from(user);
    }
}

