package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.Clinic;
import kg.oblaka.oblaka.domain.Department;
import kg.oblaka.oblaka.domain.Share;
import kg.oblaka.oblaka.dto.*;
import kg.oblaka.oblaka.repository.ClinicRepository;
import kg.oblaka.oblaka.repository.CommentRepository;
import kg.oblaka.oblaka.repository.DoctorRepository;
import kg.oblaka.oblaka.repository.ShareRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.print.Doc;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CommonServiceImpl implements CommonService {
    private final ClinicServiceImpl clinicService;
    private final DoctorServiceImpl doctorService;
    private final DepartmentServiceImpl departmentService;
    private final ShareServiceImpl shareService;
    private final CommentServiceImpl commentService;


    @Override
    public CommonDTO showAll() {
        List<ClinicDTO> clinicDTOS = clinicService.show();
        List<DoctorDTO> doctorDTOS = doctorService.show();
        List<DepartmentDTO> departmentDTOS = departmentService.show();
        List<ShareDTO> shareDTOS = shareService.show();
        List<CommentDTO> commentDTOS = commentService.show();
        return CommonDTO.toDTO(clinicDTOS, doctorDTOS, departmentDTOS, shareDTOS, commentDTOS);
    }
}
