package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.BranchDTO;
import kg.oblaka.oblaka.dto.DepartmentDTO;

import java.util.List;

public interface DepartmentService {
    List<DepartmentDTO> show();

    DepartmentDTO findById(String id);
}
