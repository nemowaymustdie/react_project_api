package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.AccommodationDTO;
import kg.oblaka.oblaka.dto.BranchDTO;

import java.util.List;

public interface BranchService {
    List<BranchDTO> show();

    BranchDTO findById(String id);
}
