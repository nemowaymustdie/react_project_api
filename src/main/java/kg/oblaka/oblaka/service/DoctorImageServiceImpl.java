package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.DoctorImage;
import kg.oblaka.oblaka.dto.DoctorImageDTO;
import kg.oblaka.oblaka.repository.DoctorImageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class DoctorImageServiceImpl implements DoctorImageService {
    private final DoctorImageRepository doctorImageRepository;


    public DoctorImageDTO upload (MultipartFile file) {
        try {
            byte[] bytes = new byte[0];

            try {
                bytes = file.getBytes();
            } catch (Exception e) {
                e.printStackTrace();
            }

            DoctorImage image = DoctorImage.builder().imageData(bytes).build();
            doctorImageRepository.save(image);

            return DoctorImageDTO.toDTO(image);
        }catch (Exception e) {
            e.printStackTrace();
        }

        return DoctorImageDTO.toDTO(DoctorImage.EMPTY);
    }

    @Override
    public Resource findByIdAsResource(String imageId) {
        DoctorImage image =  doctorImageRepository
                .findById(imageId)
                .orElseThrow(() -> new RuntimeException("No resources found"));

        return new ByteArrayResource(image.getImageData());
    }

    @Override
    public DoctorImageDTO updateById(String id, MultipartFile file) {
        Optional<DoctorImage> image =  doctorImageRepository.findById(id);


        if(!image.isEmpty()){
            DoctorImage doctorImage = image.get();
            byte[] bytes = new byte[0];

            try {
                bytes = file.getBytes();
            } catch (Exception e) {
                e.printStackTrace();
            }
            doctorImage.setImageData(bytes);
            doctorImageRepository.save(doctorImage);
            return DoctorImageDTO.toDTO(doctorImage);
        }else{
            return upload(file);
        }

    }
}
