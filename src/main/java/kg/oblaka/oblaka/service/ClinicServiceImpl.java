package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.Clinic;
import kg.oblaka.oblaka.dto.ClinicDTO;
import kg.oblaka.oblaka.repository.ClinicRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ClinicServiceImpl implements ClinicService {
    private final ClinicRepository clinicRepository;

    @Override
    public List<ClinicDTO> show() {
        return ClinicDTO.toListDTOS((List<Clinic>) clinicRepository.findAll());
    }
}
