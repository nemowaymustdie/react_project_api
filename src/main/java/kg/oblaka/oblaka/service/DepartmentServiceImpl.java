package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.Branch;
import kg.oblaka.oblaka.domain.Department;
import kg.oblaka.oblaka.dto.BranchDTO;
import kg.oblaka.oblaka.dto.DepartmentDTO;
import kg.oblaka.oblaka.repository.DepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DepartmentServiceImpl implements DepartmentService{
    private final DepartmentRepository  departmentRepository;

    @Override
    public List<DepartmentDTO> show() {
        return DepartmentDTO.toListDTOS((List<Department>) departmentRepository.findAll());
    }

    @Override
    public DepartmentDTO findById(String id) {
        return DepartmentDTO.toDTO(departmentRepository.findById(id).get());
    }
}
