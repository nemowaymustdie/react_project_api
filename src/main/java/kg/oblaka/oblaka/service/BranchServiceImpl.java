package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.Branch;
import kg.oblaka.oblaka.dto.AccommodationDTO;
import kg.oblaka.oblaka.dto.BranchDTO;
import kg.oblaka.oblaka.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;

    @Override
    public List<BranchDTO> show() {
        return BranchDTO.toListDTOS((List<Branch>) branchRepository.findAll());
    }

    @Override
    public BranchDTO findById(String id) {
        return BranchDTO.toDTO(branchRepository.findById(id).get());
    }
}
