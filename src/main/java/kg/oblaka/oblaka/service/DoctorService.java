package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.DoctorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface DoctorService {
    List<DoctorDTO> show();

    boolean add(DoctorDTO doctorDTO);

    ResponseEntity<Void> update(String id, DoctorDTO doctorDTO);

    ResponseEntity<Void> delete(String id);

    DoctorDTO showById(String id);
}
