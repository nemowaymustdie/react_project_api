package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.AccommodationDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccommodationService {
    List<AccommodationDTO> show();

    boolean add(AccommodationDTO accommodationDTO);

    ResponseEntity<Void> update(String id, AccommodationDTO accommodationDTO);

    ResponseEntity<Void> delete(String id);

    AccommodationDTO findById(String id);
}
