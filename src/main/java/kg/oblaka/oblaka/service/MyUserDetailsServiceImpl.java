package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.domain.MyUserDetails;
import kg.oblaka.oblaka.domain.User;
import kg.oblaka.oblaka.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@RequiredArgsConstructor
@Service
public class MyUserDetailsServiceImpl implements MyUserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByName(s);

        User user = optionalUser.orElseGet(() -> {
            throw  new UsernameNotFoundException("NOT FOUND");
        });

        return MyUserDetails.from(user);
    }
}
