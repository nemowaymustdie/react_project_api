package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.ShareDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ShareService {
    List<ShareDTO> show();

    boolean addShare(ShareDTO shareDTO);

    ResponseEntity<Void> update(String id, ShareDTO shareDTO);

    ResponseEntity<Void> delete(String id);
}
