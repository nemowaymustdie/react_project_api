package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.DoctorImageDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface DoctorImageService {
    Resource findByIdAsResource(String imageId);

    DoctorImageDTO updateById(String id, MultipartFile file);
}
