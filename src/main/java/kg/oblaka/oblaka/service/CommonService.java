package kg.oblaka.oblaka.service;

import kg.oblaka.oblaka.dto.CommonDTO;

import java.util.List;

public interface CommonService {
    CommonDTO showAll();
}
