package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.dto.CommentDTO;
import kg.oblaka.oblaka.service.CommentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
@RestController
@RequestMapping("/comment")
public class CommentController {
    private final CommentServiceImpl commentService;


    @GetMapping
    public List<CommentDTO> showComment(){
        return commentService.show();
    }


    @PostMapping("addComment")
    public String addComment(@RequestBody CommentDTO commentDTO){
        boolean isSaved = commentService.addComment(commentDTO);
        if(isSaved){
            return "redirect:/comment";
        }
        else {
            return "redirect:/addComment";
        }
    }

    @GetMapping("/{id}")
    public CommentDTO showCommentById(@PathVariable String id){
        return commentService.showById(id);
    }



    @PutMapping("/updateComment/{id}")
    public ResponseEntity<Void> updateComment(@PathVariable String id, @RequestBody CommentDTO commentDTO){
        return commentService.update(id, commentDTO);
    }

    @DeleteMapping("/deleteComment/{id}")
    public ResponseEntity<Void> deleteComment(@PathVariable String id){
        return commentService.delete(id);

    }



}
