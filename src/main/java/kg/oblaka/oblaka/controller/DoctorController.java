package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.dto.ClinicDTO;
import kg.oblaka.oblaka.dto.DoctorDTO;
import kg.oblaka.oblaka.dto.ShareDTO;
import kg.oblaka.oblaka.service.DoctorServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
@RestController
@RequestMapping("/doctor")
public class DoctorController {
    private final DoctorServiceImpl doctorService;

    @GetMapping
    public List<DoctorDTO> show(){
        return doctorService.show();
    }


    @GetMapping("{id}")
    public DoctorDTO showById(@PathVariable String id){

        return doctorService.showById(id);
    }

    @PostMapping("add")
    public String add(@RequestBody DoctorDTO doctorDTO){
        boolean isSaved = doctorService.add(doctorDTO);
        if(isSaved){
            return "Done!";
        }
        else {
            return "Exception!";
        }
    }




    @PutMapping("/update/{id}")
    public ResponseEntity<Void> update(@PathVariable String id, @RequestBody DoctorDTO doctorDTO){
        return doctorService.update(id, doctorDTO);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id){
        return doctorService.delete(id);

    }
}
