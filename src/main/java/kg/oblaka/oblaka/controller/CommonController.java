package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.dto.CommonDTO;
import kg.oblaka.oblaka.service.CommonServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
@RestController
@RequestMapping("/common")
public class CommonController {
    private final CommonServiceImpl commonService;

    @GetMapping
    public CommonDTO showAll(){
        return commonService.showAll();
    }
}
