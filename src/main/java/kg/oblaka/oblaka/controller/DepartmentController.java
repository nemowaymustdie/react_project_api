package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.dto.BranchDTO;
import kg.oblaka.oblaka.dto.DepartmentDTO;
import kg.oblaka.oblaka.service.BranchServiceImpl;
import kg.oblaka.oblaka.service.DepartmentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/department")
public class DepartmentController {
    private final DepartmentServiceImpl departmentService;

    @GetMapping
    public List<DepartmentDTO> show(){
        return departmentService.show();
    }

    @GetMapping("/{id}")
    public DepartmentDTO findById(@PathVariable String id) { return departmentService.findById(id);}
}
