package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.dto.ClinicDTO;
import kg.oblaka.oblaka.dto.CommentDTO;
import kg.oblaka.oblaka.dto.ShareDTO;
import kg.oblaka.oblaka.service.ShareServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(origins = "http://127.0.0.1:5500")
@RequiredArgsConstructor
@RestController
@RequestMapping("/share")
public class ShareController {

    private final ShareServiceImpl shareService;

    @GetMapping
    public List<ShareDTO> show(){
        return shareService.show();
    }

    @PostMapping("addShare")
    public String addShare(@RequestBody ShareDTO shareDTO){
        boolean isSaved = shareService.addShare(shareDTO);
        if(isSaved){
            return "Done!";
        }
        else {
            return "Exception!";
        }
    }




    @PutMapping("/updateShare/{id}")
    public ResponseEntity<Void> updateShare(@PathVariable String id, @RequestBody ShareDTO shareDTO){
        return shareService.update(id, shareDTO);
    }

    @DeleteMapping("/deleteShare/{id}")
    public ResponseEntity<Void> deleteShare(@PathVariable String id){
        return shareService.delete(id);

    }
}
