package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.dto.AccommodationDTO;
import kg.oblaka.oblaka.dto.BranchDTO;
import kg.oblaka.oblaka.dto.DoctorDTO;
import kg.oblaka.oblaka.service.AccommodationServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/accommodation")
public class AccommodationController {
    private final AccommodationServiceImpl accommodationService;

    @GetMapping
    public List<AccommodationDTO> show(){
        return accommodationService.show();
    }

    @GetMapping("/{id}")
    public AccommodationDTO findById(@PathVariable String id){
        return accommodationService.findById(id);
    }

    @PostMapping("add")
    public String add(@RequestBody AccommodationDTO accommodationDTO){
        boolean isSaved = accommodationService.add(accommodationDTO);
        if(isSaved){
            return "Done!";
        }
        else {
            return "Exception!";
        }
    }




    @PutMapping("/update/{id}")
    public ResponseEntity<Void> update(@PathVariable String id, @RequestBody AccommodationDTO accommodationDTO){
        return accommodationService.update(id, accommodationDTO);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id){
        return accommodationService.delete(id);

    }

}
