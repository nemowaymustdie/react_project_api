package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.domain.AuthenticationRequest;
import kg.oblaka.oblaka.dto.UserDTO;
import kg.oblaka.oblaka.repository.UserRepository;
import kg.oblaka.oblaka.service.UserService;
import kg.oblaka.oblaka.service.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserServiceImpl service;


    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO createUser (@RequestBody UserDTO userData) {
        return service.save(userData);
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<?> authenticateUser (@RequestBody AuthenticationRequest request) throws Exception {
        return service.authenticate(request);
    }
}

