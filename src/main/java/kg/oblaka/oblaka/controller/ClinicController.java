package kg.oblaka.oblaka.controller;


import kg.oblaka.oblaka.domain.Clinic;
import kg.oblaka.oblaka.dto.ClinicDTO;
import kg.oblaka.oblaka.service.ClinicServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//@CrossOrigin(origins = "http://localhost:3004")
@RequiredArgsConstructor
@RestController
@RequestMapping("/clinic")
public class ClinicController {
    private final ClinicServiceImpl clinicService;

    @GetMapping
    public List<ClinicDTO> show(){
        return clinicService.show();
    }




}
