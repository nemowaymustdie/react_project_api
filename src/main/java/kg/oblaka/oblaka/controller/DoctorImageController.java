package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.domain.DoctorImage;
import kg.oblaka.oblaka.dto.DoctorImageDTO;
import kg.oblaka.oblaka.service.DoctorImageServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
@RestController
@RequestMapping("/doctorImages")
public class DoctorImageController {
    private final DoctorImageServiceImpl service;

    @GetMapping("/{imageId}")
    public ResponseEntity<Resource> getById (@PathVariable String imageId) {
        Resource image = service.findByIdAsResource(imageId);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE)
                .body(image);
    }

    @PostMapping("/upload")
    public DoctorImageDTO upload (@RequestParam("file") MultipartFile file) {
        return service.upload(file);
    }

    @PutMapping("/update/{id}")
    public DoctorImageDTO updateById (@PathVariable String id, @RequestParam("file") MultipartFile file) {
        return service.updateById(id, file);
    }

}
