package kg.oblaka.oblaka.controller;

import kg.oblaka.oblaka.dto.AccommodationDTO;
import kg.oblaka.oblaka.dto.BranchDTO;
import kg.oblaka.oblaka.service.BranchServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/branch")
public class BranchController {
    private final BranchServiceImpl branchService;

    @GetMapping
    public List<BranchDTO> show(){
        return branchService.show();
    }


    @GetMapping("/{id}")
    public BranchDTO findById(@PathVariable String id){
        return branchService.findById(id);
    }

}
